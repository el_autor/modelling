import Foundation

func mainFunc(x: Double, y: Double) -> Double {
    return x * x + y * y
}

func euler(xn: Double, yn: Double, h: Double) -> Double {
    return yn + h * mainFunc(x: xn,
                             y: yn)
}

func rungeKutta(xn: Double, yn: Double, h: Double) -> Double {
    let alpha: Double = 1
    
    let k1 = mainFunc(x: xn,
                      y: yn)
    let k2 = mainFunc(x: xn + h / (2 * alpha),
                      y: yn + h / (2 * alpha) * k1)
    
    return yn + h * ((1 - alpha) * k1 + alpha * k2)
}

func picard1(x: Double) -> Double {
    return pow(x, 3) / 3
}

func picard2(x: Double) -> Double {
    return picard1(x: x) * (pow(x, 4) * (1 / 21) + 1)
}

func picard3(x: Double) -> Double {
    return picard1(x: x) * (pow(x, 12) * (1 / 19845) + 2 * pow(x, 8) * (1 / 693)) + picard2(x: x)
}

func picard4(x: Double) -> Double {
    return picard1(x: x) * (pow(x, 28) * (1 / 36_625_634_325) + 4 * pow(x, 24) * (1 / 1_113_959_385) + 662 * pow(x, 20) * (1 / 3_479_404_005) + 82 * pow(x, 16) * (1 / 12_442_815) + 4 * pow(x, 12) * (1 / 31185)) + picard3(x: x)
}

func getValue(method: (_ x: Double) -> Double,
              start: Double,
              end: Double,
              step: Double) -> ([Double], [Double]) {
    
    var result: ([Double], [Double]) = ([0], [0])
    
    for i in stride(from: start + step, through: end + step, by: step) {
        let value = method(i)
        
        result.0.append(i)
        result.1.append(value)
    }
    
    return result
}

func getValue(method: (Double, Double, Double) -> Double,
              start: Double,
              end: Double,
              step: Double) -> ([Double], [Double]) {
    var result: ([Double], [Double]) = ([0], [0])
    
    for i in stride(from: start + step, through: end + step, by: step) {
        let value = method(i - step, result.1.last!, step)
        
        result.0.append(i)
        result.1.append(value)
    }
    
    return result
}
