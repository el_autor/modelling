import SwiftyTextTable

print("Введите старт диапазона:")
let start = Double(readLine()!)!

print("Введите конец диапазона:")
let end = Double(readLine()!)!

print("Введите шаг:")
let step = Double(readLine()!)!

let x = TextTableColumn(header: "x")
let picard1Col = TextTableColumn(header: "Picard (1st)")
let picard2Col = TextTableColumn(header: "Picard (2st)")
let picard3Col = TextTableColumn(header: "Picard (3st)")
let picard4Col = TextTableColumn(header: "Picard (4st)")
let eulerCol = TextTableColumn(header: "Euler")
let rungeKuttaCol = TextTableColumn(header: "Runge-Kutta (2)")


var table = TextTable(columns: [x,
                                picard1Col,
                                picard2Col,
                                picard3Col,
                                picard4Col,
                                eulerCol,
                                rungeKuttaCol])

let picard1Values = getValue(method: picard1, start: start, end: end, step: step).1
let picard2Values = getValue(method: picard2, start: start, end: end, step: step).1
let picard3Values = getValue(method: picard3, start: start, end: end, step: step).1
let picard4Values = getValue(method: picard4, start: start, end: end, step: step).1
let eulerValues = getValue(method: euler, start: start, end: end, step: step).1
let rungeKuttaValues = getValue(method: rungeKutta, start: start, end: end, step: step).1

var index = 0

for i in stride(from: start, to: end + step, by: step) {
    table.addRow(values: [String(format: "%.5f", i),
                              String(format: "%.10f", picard1Values[index]),
                              String(format: "%.10f", picard2Values[index]),
                              String(format: "%.10f", picard3Values[index]),
                              String(format: "%.10f", picard4Values[index]),
                              String(format: "%.10f", eulerValues[index]),
                              String(format: "%.10f", rungeKuttaValues[index])])
    
    index += 1
}

print(table.render())
